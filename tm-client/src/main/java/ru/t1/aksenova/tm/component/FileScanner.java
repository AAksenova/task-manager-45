package ru.t1.aksenova.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class FileScanner {

    @NotNull
    private final List<String> commands = new ArrayList<>();
    @NotNull
    private final File folder = new File("./");
    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    @Nullable
    private Bootstrap bootstrap;

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (@NotNull final File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                } catch (@NotNull final Exception e) {
                    bootstrap.getLoggerService().error(e);
                }
            }
        }
    }

    public void stop() {
        es.shutdown();
    }

}
