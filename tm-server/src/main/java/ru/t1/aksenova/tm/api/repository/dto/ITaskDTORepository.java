package ru.t1.aksenova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.enumerated.TaskSort;

import java.util.Comparator;
import java.util.List;

public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId, @NotNull TaskSort sort);

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId, @NotNull Comparator<TaskDTO> comparator);

    @Nullable
    TaskDTO findOneById(@NotNull String id);

    @Nullable
    TaskDTO findOneById(@NotNull String id, @NotNull String userId);

    void removeById(@NotNull String id);

    void removeById(@NotNull String id, @NotNull String userId);

    void clear();

    void clear(@NotNull String userId);

    long getCount();

    long getCount(@NotNull String userId);

    boolean existById(@NotNull String id);

    boolean existById(@NotNull String id, @NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTaskByProjectId(@NotNull String projectId);
}
