package ru.t1.aksenova.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ApplicationAboutResponse extends AbstractResponse {

    @Nullable
    private String email;

    @Nullable
    private String name;

}
